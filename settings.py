from raw.settings import *

def load(filename):
    with open(filename, 'r') as f:
        return CreateFromDocument(f.read())
