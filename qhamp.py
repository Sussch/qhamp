#!/usr/bin/python

from PySide import QtGui, QtCore, QtXml
import logging
import optparse
import os

from window import MainWindow


if __name__ == '__main__':
    import sys
    
    optp = optparse.OptionParser()
    optp.add_option('-v', '--verbose', dest='verbose', action='count',
                    help="Increase verbosity (specify multiple times for more)")
    opts, args = optp.parse_args()

    # Here would be a good place to check what came in on the command line and
    # call optp.error("Useful message") to exit if all it not well.

    log_level = logging.WARNING # default
    if opts.verbose == 1:
        log_level = logging.INFO
    elif opts.verbose >= 2:
        log_level = logging.DEBUG
    
    logging.basicConfig(level=log_level, filename='qhamp.log', 
        format='%(asctime)s: %(levelname)s: %(message)s')
    log = logging.getLogger(__name__)
    # NOTE:: basicConfig does not set up StreamHandler when supplied with a filename
    # Add logging to stdout, stderr
    streamhandler = logging.StreamHandler()
    streamhandler.setLevel(log_level)
    log.addHandler(streamhandler)
    
    log.info("\nQHamp launched")
    
    app = QtGui.QApplication(sys.argv)
    
    try:
        mainwin = MainWindow()
        mainwin.show()
    except Exception as e:
        mainwin = QtGui.QMainWindow()
        errorDialog = QtGui.QErrorMessage(mainwin)
        errorDialog.setWindowTitle("Error")
        errorDialog.showMessage(str(e))
        log.exception(e)

    app.connect(app, QtCore.SIGNAL("lastWindowClosed()"),
                app, QtCore.SLOT("quit()"))
    app.exec_()
