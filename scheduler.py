from PySide import QtGui, QtCore
import logging

import settings
from channel import ChannelState
from measurements import Measurements
from interface import HMPInterface

class HMPUpdateScheduler(QtCore.QObject):
    sig_measurements = QtCore.Signal(object)

    def __init__(self, parent, settings):
        super(HMPUpdateScheduler, self).__init__()
        self.parent = parent
        self.settings = settings
        self.running = False
        self.hmp_interface = None
        self.log = logging.getLogger(__name__)
        self.log.info("HMP Update will be called every {} milliseconds.".format(self.settings.poll_period))
        
        self.t = QtCore.QThread()
        self.moveToThread(self.t)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self._do_update)
        self.timer.setInterval(self.settings.poll_period)
        self.timer.moveToThread(self.t)
        self.t.started.connect(self.timer.start)
        self.t.finished.connect(self.timer.stop)

    def _do_update(self):
        if self.running and self.hmp_interface != None:
            m = Measurements()
            # For all channels / terminals
            for i in range(0, self.settings.num_terminals):
                # Select it, measure voltage and current
                self.hmp_interface.set_active_channel(i + 1)
                m.set_voltage(i, self.hmp_interface.get_voltage())
                m.set_current(i, self.hmp_interface.get_current())
            
            self.sig_measurements.emit(m)
    
    @QtCore.Slot(ChannelState)
    def set_enabled(self, chstate):
        if self.hmp_interface != None:
            if chstate.name == chstate.MASTER:
                self.hmp_interface.set_master_enabled(chstate.enabled)
            else:
                index = int(chstate.name) + 1
                self.hmp_interface.set_channel_enabled(index, chstate.enabled)
        
    def start(self):
        self.hmp_interface = HMPInterface(self.settings.port)
        self.log.info("HMP update scheduler started")
        self.running = True
        self.t.start()

    def quit(self):
        self.t.quit()
        self.running = False
        self.hmp_interface = None
