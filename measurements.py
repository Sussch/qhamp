from PySide import QtCore

class Measurements(QtCore.QObject):
    def __init__(self):
        self.voltages = {}
        self.currents = {}

    def get_voltage(self, index):
        return self.voltages[index]

    def set_voltage(self, index, voltage):
        self.voltages[index] = voltage
        
    def get_current(self, index):
        return self.currents[index]
        
    def set_current(self, index, current):
        self.currents[index] = current
