from PySide import QtGui, QtCore
import os, sys
import logging

import settings
from measurements import Measurements
from channel import ChannelWidget, ChannelState
from scheduler import HMPUpdateScheduler

class MainWindow(QtGui.QWidget):
    sig_set_enabled = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        
        self.log = logging.getLogger(__name__)
        
        # Needed for a frozen PyInstaller package
        if getattr(sys, 'frozen', False):
            self.basedir = os.path.dirname(sys.executable)
            self.log.info("Running from a frozen PyInstaller package")
        elif __file__:
            self.basedir = os.path.dirname(__file__)
            
        self.configdir = os.path.join(self.basedir, 'config')
        self.load_config()
        
        self.hmp_scheduler = None
        
        # Why doesn't this work?
        self.main_layout = QtGui.QVBoxLayout()
        
        self.outp_enable_buttons = {}
        self.outp_channels = {}
        self.sigmapper = QtCore.QSignalMapper(self)        
        self.connect(QtCore.SIGNAL('clicked()'), self.sigmapper, QtCore.SLOT('map()'))
        
        self.build_enable_buttons()
        self.build_channel_widgets()

        self.setLayout(self.main_layout)
        self.setWindowTitle("Qt Hameg power supply controller")
        self.adjustSize()
        
        self.start_interface()

    def load_config(self):
        self.log.info("Loading configuration from XML files...")
        self.settings = settings.load(os.path.join(self.configdir, 'settings.xml'))
        self.log.info("Finished loading configuration from files.")
    
    def start_interface(self):
        self.log.info("Starting HMP Interface on port {}".format(self.settings.port))
        self.hmp_scheduler = HMPUpdateScheduler(self, self.settings)
        # Connect signals
        self.hmp_scheduler.sig_measurements.connect(self.got_measurements)
        self.sig_set_enabled.connect(self.hmp_scheduler.set_enabled)
        # Start
        self.hmp_scheduler.start()
    
    def stop_interface(self):
        self.log.info("Stopping HMP Interface")
        self.hmp_scheduler.quit()
        self.hmp_scheduler = None

    def build_enable_buttons(self):
        layout = QtGui.QHBoxLayout()
        # Generate output enable buttons
        for i in range(0, self.settings.num_terminals):
            button = QtGui.QPushButton('CH' + str(i + 1))
            button.setCheckable(True)
            self.sigmapper.setMapping(button, i)
            button.clicked.connect(self.sigmapper.map)
            button.setProperty('index', i)

            self.outp_enable_buttons[i] = button
            layout.addWidget(button)
        self.sigmapper.mapped.connect(self.outp_enable_clicked)
        
        button = QtGui.QPushButton("Output")
        button.setCheckable(True)
        self.master_enable_button = button
        button.clicked.connect(self.master_enable_clicked)
        layout.addWidget(button)
        
        self.main_layout.addLayout(layout)
    
    def build_channel_widgets(self):
        layout = QtGui.QHBoxLayout()
        # Generate terminal channel widgets
        for i in range(0, self.settings.num_terminals):
            channel = ChannelWidget(self)
            channel.set_index(i)

            self.outp_channels[i] = channel
            layout.addWidget(channel)
        
        self.main_layout.addLayout(layout)

    def outp_enable_clicked(self, terminal):
        if terminal in self.outp_enable_buttons:
            is_checked = self.outp_enable_buttons[terminal].isChecked()
            cs = ChannelState(str(terminal), is_checked)
            
            self.sig_set_enabled.emit(cs)
            
    def master_enable_clicked(self):
        is_checked = self.master_enable_button.isChecked()
        cs = ChannelState(ChannelState.MASTER, is_checked)
        
        self.sig_set_enabled.emit(cs)
    
    @QtCore.Slot(Measurements)
    def got_measurements(self, measurements):
        for i in range(0, self.settings.num_terminals):
            self.outp_channels[i].set_voltage(measurements.voltages[i])
            self.outp_channels[i].set_current(measurements.currents[i])

    def closeEvent(self, event):
        self.log.info("Closing..")
        self.stop_interface()
