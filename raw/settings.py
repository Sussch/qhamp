# ./raw/settings.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:e92452c8d3e28a9e27abfc9994d2007779e7f4c9
# Generated 2013-05-09 14:02:00.735194 by PyXB version 1.2.0
# Namespace AbsentNamespace0

import pyxb
import pyxb.binding
import pyxb.binding.saxer
import StringIO
import pyxb.utils.utility
import pyxb.utils.domutils
import sys

# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:dfb0c200-b897-11e2-b8f7-001fe1c6c253')

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

Namespace = pyxb.namespace.CreateAbsentNamespace()
Namespace.configureCategories(['typeBinding', 'elementBinding'])
ModuleRecord = Namespace.lookupModuleRecordByUID(_GenerationUID, create_if_missing=True)
ModuleRecord._setModule(sys.modules[__name__])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.
    
    @kw default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    saxer.parse(StringIO.StringIO(xml_text))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """General settings."""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 7, 4)
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element port uses Python identifier port
    __port = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'port'), 'port', '__AbsentNamespace0_CTD_ANON_port', False, pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 9, 8), )

    
    port = property(__port.value, __port.set, None, u'Serial port, which the supply is connected to.')

    
    # Element poll_period uses Python identifier poll_period
    __poll_period = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'poll_period'), 'poll_period', '__AbsentNamespace0_CTD_ANON_poll_period', False, pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 14, 8), )

    
    poll_period = property(__poll_period.value, __poll_period.set, None, u'Period for polling the supply.')

    
    # Element num_terminals uses Python identifier num_terminals
    __num_terminals = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(None, u'num_terminals'), 'num_terminals', '__AbsentNamespace0_CTD_ANON_num_terminals', False, pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 19, 8), )

    
    num_terminals = property(__num_terminals.value, __num_terminals.set, None, u'Number of supply terminals.')


    _ElementMap = {
        __port.name() : __port,
        __poll_period.name() : __poll_period,
        __num_terminals.name() : __num_terminals
    }
    _AttributeMap = {
        
    }



settings = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, u'settings'), CTD_ANON, documentation=u'General settings.', location=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 3, 2))
Namespace.addCategoryObject('elementBinding', settings.name().localName(), settings)



CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'port'), pyxb.binding.datatypes.string, scope=CTD_ANON, documentation=u'Serial port, which the supply is connected to.', location=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 9, 8)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'poll_period'), pyxb.binding.datatypes.unsignedInt, scope=CTD_ANON, documentation=u'Period for polling the supply.', location=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 14, 8)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(None, u'num_terminals'), pyxb.binding.datatypes.unsignedByte, scope=CTD_ANON, documentation=u'Number of supply terminals.', location=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 19, 8)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it's invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0L, max=1L, metadata=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 9, 8))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0L, max=1L, metadata=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 14, 8))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0L, max=1L, metadata=pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 19, 8))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, u'port')), pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 9, 8))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, u'poll_period')), pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 14, 8))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(None, u'num_terminals')), pyxb.utils.utility.Location('/home/sussch/Documents/projects/estcube/workspace/qhamp/schemas/settings.xsd', 19, 8))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton()

