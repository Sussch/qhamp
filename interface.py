import serial
import time

class HMPInterface:
    def __init__(self, port):
        self.serial = serial.Serial(port, 9600, timeout=1)
        self.running = True
        #self.rxbuf = ''
    
    def __del__(self):
        self.running = False
        if self.serial != None:
            #self.serial.flush()
            self.serial.close()
    
    # Gets supply identification number and version
    def get_id(self):
        return self.send_request("*IDN?")
        
    # Get the index of the active channel
    def get_active_channel(self):
        return int(self.send_request("INST:NSEL?"))
        
    # Activates a channel
    def set_active_channel(self, index):
        self.send_command("INST OUTP{:d}".format(index))
        
    # Enables output on a terminal
    def set_channel_enabled(self, index=-1, enabled=True):
        if index >= 0:
            self.set_active_channel(index)
        if enabled:
            self.send_command("OUTP:SEL ON")
        else:
            self.send_command("OUTP:SEL OFF")
            
    # Master output enable
    def set_master_enabled(self, enabled=True):
        if enabled:
            self.send_command("OUTP:GEN ON")
        else:
            self.send_command("OUTP:GEN OFF")
        
    # Gets voltage output on a channel
    def get_voltage(self, index=-1):
        if index >= 0:
            self.set_active_channel(index)
        return float(self.send_request("MEAS:VOLT?"))
        
    # Gets current output on a channel
    def get_current(self, index=-1):
        if index >= 0:
            self.set_active_channel(index)
        return float(self.send_request("MEAS:CURR?"))
    
    # Read a line-feed terminated response from the supply
    def read_till_lf(self):
        #if self.serial.inWaiting() > 0:
        #    self.rxbuf = self.rxbuf + self.serial.read(self.serial.inWaiting())
        response = ""
        x = ''
        while x != '\n': # TODO:: Need a timeout here
            if not self.running:
                break
            x = self.serial.read(1)
            response = response + x
        return response
    
    # Send a command to the supply
    def send_command(self, command):
        self.serial.write(command + '\n')
        time.sleep(0.1)
    
    # Send a request to the supply, waiting for the response
    def send_request(self, request):
        self.serial.write(request + '\n')
        return self.read_till_lf()
