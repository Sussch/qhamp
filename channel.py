from PySide import QtGui, QtCore, QtUiTools
import os, sys

class ChannelState(QtCore.QObject):
    MASTER = 'master'

    def __init__(self, name=MASTER, enabled=True):
        self.name = name
        self.enabled = enabled

class ChannelWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ChannelWidget, self).__init__(parent)
        
        self.index = 0
        
        # Needed for a frozen PyInstaller package
        if getattr(sys, 'frozen', False):
            self.basedir = os.path.dirname(sys.executable)
        elif __file__:
            self.basedir = os.path.dirname(__file__)
        
        # Load widget contents from file
        file = QtCore.QFile(os.path.join(self.basedir, "ui/channel.ui"))
        file.open(QtCore.QFile.ReadOnly)
        self.content = QtUiTools.QUiLoader().load(file, self)
        file.close()

    def set_index(self, index):
        self.index = index
        self.content.group.setTitle(str(index))
    
    def set_voltage(self, voltage):
        self.content.voltage.setValue(voltage)
    
    def set_current(self, current):
        self.content.current.setValue(current)
